// World worst IRC Bot ~ By Fudgie
// To configure please copy the sample config to the file config.json and edit to suit.

// Load those sweet depends.
const irc = require('irc');
const fs = require('fs');
const request = require('request');
const Promise = require('promise');
const vm = require('vm');

// Some libs of our own?
const Database = require('./lib/database.js');
const Log = require('./lib/Log.js');

class PaperBot {
	constructor(){
		
		// DO NOT CHANGE ANYTHING BELOW
		// ANYTHING THAT CAN BE CHANGED IS PRESENT IN THE CONFIGURATION
		this.version = "2.0";
		this.debug = false;
		this.configuration = {};
		this.keys = {};
		this.clients = {};
		this.triggers = {};
		this.aliases = {};
		this.lib = {};
		this.regex = [];
		this.log = new Log("CORE");
		this.database = new Database();
		this.root = (process.env.user == 'root' ? true : false);
		this.contexts = [];
		
		// Setup Logger
		if (!fs.existsSync("log")) {
			fs.mkdirSync("log");
		}
		
		this.logFile = fs.createWriteStream("log/latest.log");
		this.log.on("all", (type, line) => {
			this.logFile.write(`${line}\r\n`);
		});
	}
	
	loadConfig(cb){
		fs.readFile('data/config.json',(err,data)=>{
			if (err) {
				cb(false,err);
			} else {
				try {
					this.configuration = JSON.parse(data);
					if (typeof this.configuration['keys'] != "undefined") this.keys = this.configuration.keys;
					if (typeof this.configuration['dev'] != "undefined") this.debug = this.configuration.dev;
					cb(true);
				} catch(e) {
					cb(false,e);
				}
			}
		});
	}
	loadBots(cb){
		for (let sid in this.configuration['servers']) {
			const server = this.configuration['servers'][sid];
			if (!server.enabled) continue;
			
			let channels = [];
			for (let ch in server.channels) {
				let chan = server.channels[ch];
				if (chan[0] == "!" && this.debug) {
					channels.push(chan.substr(1));
				} else if (!this.debug) {
					channels.push(chan);
				}
			}
			
			const client = new irc.Client(server.host,server.nick,{port:server.port,channels:channels,autoConnect:false,secure:server.ssl});
			client.verbosity = 0;
			client.trigger = server.trigger;
			
			const nid = sid;
			
			client.correctNick = function(nick){
				for (let ch in client.chans) {
					let channel = client.chans[ch];
					for (let n in channel.users) {
						if (n.toLowerCase() == nick.toLowerCase()) return n;
					}
				}
				return nick;
			}
			client.on('join', (channel, nick, message) => {
				if (!this.debug) return;
				if (nick.toLowerCase() == client.nick.toLowerCase()) client.say(channel,"Running in debug/devel mode.");
			});
			
			client.on('registered',()=>{
				this.log.info("Connected to "+nid+" as "+server.nick+"!");
			});
			client.addListener('message',(from,to,text)=>{
				
				// Log the message
				
				// Current channel
				const endpoint = `${nid}:${to.toLowerCase()}`;
				this.log.info(`[${endpoint}] (${from}) ${text}`);
				
				// Toss the message to triggers and regex etc
				var message = text.split(' ');
				if (message.length <= 0) return;
				if (message[0].trim() == "") return;
				
				let payload = {client:client,from:from,to:to,message:message};
				
				var tr = message[0].trim().toLowerCase();
				if (tr[0].toLowerCase() == client.trigger.toLowerCase()) {
					if (tr.length == 1) {
						tr = message[1];
						message.splice(0,2);
					} else {
						tr = tr.substr(1);
						message.splice(0,1);
					}
					payload.trigger = tr;
					payload.message = message;
					try {
						if (typeof this.aliases[tr] != "undefined") tr = this.aliases[tr];
						if (typeof this.triggers[tr] != "undefined") this.triggers[tr](payload);
					} catch(e) {
						console.log(e);
					}
				}
				Promise.all(this.regex.map((obj)=>{
					var matches = text.match(obj.regex);
					payload.matches = matches;
					if (matches) obj.callback(payload);
					return true;
				}));
				
			});
			client.addListener('error', message => {
				console.log('error: ', message);
			});
			client.addListener('selfMessage',(to,text) => {
				const endpoint = `${nid}:${to.toLowerCase()}`;
				this.log.info(`[${endpoint}] (${client.nick}) ${text}`);
			});
			this.clients[sid.toLowerCase()] = client;
		}
		cb();
	}
	loadScripts() {
		this.log.info(" = Loading scripts =");
		return new Promise((resolve,reject)=>{
			fs.readdir("./scripts",(err,files)=>{
				if (err) {
					reject(err);
				} else {
					resolve(files);
				}
			});
		}).then((files)=>{
			return Promise.all(files.map((file) => {
				try {
					const context = vm.createContext({Bot:this,require:require,process:process,console:console,Log:new Log(file)});
					this.contexts.push(context);
					const script = new vm.Script(fs.readFileSync("./scripts/"+file).toString(), {filename:__dirname+"/scripts/"+file});
					const ret = script.runInContext(context);
					this.log.info("Loaded script '%s'",file);
				} catch (e) {
					console.log(e);
					return false;
				}
				return true;
			})).done(()=>{
				this.log.info(" - Finished loading scripts -");
			});
		});
	}
	reloadScripts() {
		this.triggers = {};
		this.aliases = {};
		this.lib = {};
		this.regex = [];
		return this.loadScripts();
	}
	boot(){
		this.log.info(` = Starting PaperBot v${this.version} =`);
		var bootPromise = new Promise((resolve,reject)=>{
			this.loadConfig((result,err)=>{
				if (!result) {
					reject(err);
				} else {
					this.log.info(" - Loaded Configuration -");
					resolve(resolve,reject);
				}
			});
		}).then((resolve,reject)=>{
			this.loadBots(()=>{
				resolve(resolve,reject);
			});
		}).then((resolve,reject)=>{
			return this.loadScripts();
		}).catch((err)=>{
			console.log(err);
		}).done(()=>{
			this.log.info(" =!= PaperBot is ready! =!=");
			this.log.info(" = Connecting to IRC = ");
			for (var bid in this.clients) {
				this.log.info("Connecting to '%s'",bid);
				this.clients[bid].connect();
			}
		});
	}
	defineTrigger(trigger,alias,callback){
		this.log.info("Registered trigger '%s'",trigger.toLowerCase());
		this.triggers[trigger.toLowerCase()] = callback;
		if (alias==null) alias=[];
		alias.map((a)=>{
			this.addAlias(trigger,a);
		});
	}
	addAlias(trigger,alias){
		if (this.triggers[trigger.toLowerCase()] != null) {
			this.log.info("Registered alias '%s' for trigger '%s'",alias.toLowerCase(),trigger.toLowerCase());
			this.aliases[alias.toLowerCase()] = trigger.toLowerCase();
		}
	}
	defineRegex(regex,callback) {
		this.log.info("Registered regex '%s'",regex);
		this.regex.push({regex:regex,callback:callback});
	}
	addListener(ev,callback) {
		for (var sid in this.clients) {
			const client = this.clients[sid];
			client.addListener(ev,function(channel,nick,message){
				const arr = [].slice.call(arguments);
				arr.unshift(client);
				callback.apply(null,arr);
			});
		}
	}
	isAdmin(client,nick) {
		return new Promise((resolve,reject)=>{
			client.whois(nick,(data)=>{
				resolve(data);
			});
		}).then((data)=>{
			const sid = this.getSid(client);
			if (sid==null) return Promise.reject("Unable to retrieve SID!");
			
			var masks = [];
			// Most secure
			masks.push(`${data.nick}!${data.user}@${data.host}`);
			masks.push(`${data.nick}!*@${data.host}`);
			masks.push(`*!${data.user}@${data.host}`);
			masks.push(`!${data.user}@${data.host}`);
			masks.push(`${data.user}@${data.host}`);
			
			// Kinda secure
			masks.push(`${data.nick}!${data.user}@*`);
			masks.push(`*@${data.host}`);
			masks.push(`@${data.host}`);
			masks.push(`${data.user}@*`);
			
			// Stop, drop and fucking kill yourself.
			masks.push(`${data.nick}!*@*`);
			masks.push(`${data.nick}`);
			masks.push(`*@*`);
			masks.push(`*!*@*`);
			
			const admins = this.configuration.servers[sid].admins;
			if (this.debug) console.log(admins);
			
			for (let m in masks) {
				m = masks[m];
				for (let admin in admins) {
					if (!admins.hasOwnProperty(admin)) continue;
					const a = admins[admin];
					if (this.debug) console.log(admin," : ",a," = ? = ",m);
					if (a.equalsIgnoreCase(m)) return true;
				}
			}
			
			return false;
		});
	}
	getSid(client) {
		for (var sid in this.clients) {
			if (this.clients[sid] == client) return sid;
		}
		return null;
	}
	shutdown(reason) {
		console.log("Performing shutdown.");
		if (Bot) {
			for (let nid in Bot.clients) {
				let client = Bot.clients[nid];
				if (typeof reason == "undefined") {
					client.disconnect("Goodbye!");
				} else {
					client.disconnect(reason);
				}
			}
		}
	}
	restart() {
		this.shutdown("restart");
		process.exit(12);
	}
}
const Bot = new PaperBot();
try {
	Bot.boot();
} catch(e) {
	console.log(e);
}

process.on('SIGUSR2', function () {
	console.log("Attempting graceful restart!");
	Bot.shutdown("restart");
	process.exit(12);
});
process.on('SIGINT', function () {
	console.log("Attempting graceful shutdown!");
	Bot.shutdown();
	process.exit(0);
});

// Custom prototypes
Object.defineProperty(String.prototype, "equalsIgnoreCase", {
	enumerable: false,
	value: function(comp) {
		return this.toString().toLowerCase() === comp.toString().toLowerCase();
	}
});