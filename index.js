// PaperBot Start script
// This script allows PaperBot to restart upon command.
// Starting bot.js on its own will not enable the restart functionality.

const spawn = require('child_process').spawn;

let fails = 0;

function startBot() {
	const bot = spawn('node', ['bot.js']);

	bot.stdout.on('data', (data) => {
	  console.log(data.toString().trim());
	});

	bot.stderr.on('data', (data) => {
		console.log(`stderr: ${data}`);
		fails++;
		if (fails < 3) {
			//startBot();
		} else {
			console.log("Bot has failed too many times. Exiting fully.");
		}
	});

	bot.on('close', (code) => {
		if (code == 0 || code == 12) fails = 0;
		if (code == 12) startBot();
	});
}
startBot();