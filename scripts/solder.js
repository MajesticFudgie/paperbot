const request = require("request-promise");

let packs = {};

let solder = "http://solder.thomas-edwards.me/api/";

Bot.defineTrigger("solder", null, ev => {
	const {client,message,from,to} = ev;
	
	if (message.length <= 0) {
		client.say(to,"N/A");
		return;
	}
	
	switch (message[0].toLowerCase()) {
		case 'packs':
			request({url:`${solder}modpack`,json:true}).then( data => {
				const packs = data.modpacks;
				client.say(to,`I found ${Object.keys(packs).length} packs: ${Object.values(packs).join(", ")}`);
			}).catch( data => {
				client.say(to,`Error: ${data.error}`);
			});
			break;
		case 'pack':
			if (message.length <= 1) {
				client.say(to,`Usage: ${client.trigger}solder pack [pack-name] [build]`);
			}
			const url = `${solder}modpack/${message.splice(1).join(" ")}`;
			console.log(url);
			request({url:url,json:true}).then( data => {
				if (data.error) {
					client.say(to,`Error: ${data.error}`);
					return;
				}
				client.say(to,`${data.display_name}: Recommended Build: ${data.recommended}, Latest Build: ${data.latest}, Availiable Builds: ${data.builds.join(" ,")}`);
				const pack = data;
				if (message.length >= 3) {
					request({url:`${solder}modpack/${message[1]}/${message[2]}`,json:true}).then( data => {
						if (data.error) {
							client.say(to,`Error: ${data.error}`);
							return;
						}
						client.say(to,`${pack.display_name} (${message[2]}): Minecraft Version: ${data.minecraft}, Java Version: ${data.java}, Memory Requirement: ${data.memory}MB, Mods: ${data.mods.length}`);
					}).catch(data => {
						client.say(to,`Error: ${data.error}`);
					});
				}
				
			}).catch( data => {
				client.say(to,`Error: ${data.error}`);
			});
			
			break;
		case 'server':
			if (message.length <= 1) {
				client.say(to,`Usage: ${client.trigger}solder server [server api url]`);
				return;
			}
			solder = message[1];
			client.say(to,`Solder API URL Changed to ${message[1]}`);
			break
		default:
			client.say(to,"N/A");
			break;
	}
});

Object.defineProperty(Object.prototype, "values", {
	enumerable: false,
	value: function(obj) {
		return Object.keys(obj).map((k) => obj[k]);
	}
});