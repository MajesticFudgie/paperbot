// Bot admin tools.
Bot.defineTrigger("join", ['j'], ev => {
	const {client,message,from,to} = ev;
	
	//if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			client.join("#"+message[0]);
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});

Bot.defineTrigger("reload", null, ev => {
	const {client,message,from,to} = ev;
	
	//if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			client.say(to,"Attempting hacky reload..");
			Bot.reloadScripts().then(()=>{
				client.say(to,"Done");
			});
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});

Bot.defineTrigger("exit", null, ev => {
	const {client,message,from,to} = ev;
	
	//if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			process.exit(0);
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});
Bot.defineTrigger("restart", null, ev => {
	const {client,message,from,to} = ev;
	
	//if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			Bot.restart();
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});

Bot.defineTrigger("part", ['p'], ev => {
	const {client,message,from,to} = ev;
	
	//if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			if (message.length > 0) {
				client.part("#"+message[0]);
			} else {
				client.part(to);
			}
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});

Bot.defineTrigger("kick", ['k'], ev => {
	const {client,message,from,to} = ev;
	
	if (to[0]!="#") return;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (r) {
			if (message.length > 0) {
				client.send("kick",to,message[0]);
			} else {
				client.say(to,"fuq it");
				client.send("kick",to,client.nick);
			}
		} else {
			return Promise.reject("Not Admin");
		}
	})
	.catch((e)=>{
		client.say(to,"Error: "+e);
	});
});

Bot.defineTrigger("alias", null, ev => {
	const {client,message,from,to} = ev;
	
	if (message.length >=2) {
		if (Bot.triggers[message[0].toLowerCase()] != "undefined") {
			client.say(to,`Added alias '${message[0]}' for '${message[1]}'`);
			Bot.addAlias(message[1],message[0]);
		}
	} else {
		client.say(to,`Syntax: alias [name] [trigger]`);
	}
});
Bot.defineTrigger("+admin", null, ev => {
	const {client,message,from,to} = ev;
	
	Bot.isAdmin(client,from)
	.then((r)=>{
		if (!r) return Promise.reject("Not Admin!");
		return Promise.resolve();
	})
	.then(()=>{
		const sid = Bot.getSid(client);
		if (sid == null) return Promise.reject("Unable to get SID");
		
		if (message.length <= 0) {
			client.say(to,"Syntax: +admin [hostmask]");
		} else {
			Bot.configuration.servers[sid].admins.push(message[0]);
			client.say(to,`Added ${message[0]} as an admin`);
		}
	})
	.catch((e)=>{
		client.say(to,`Error: ${e}`);
	});
});
Bot.defineTrigger("admin",null, ev => {
	const {client,message,from,to} = ev;
	
	Bot.isAdmin(client,from)
	.then( r => {
		if (!r) {
			client.say(to,`Sorry ${from}, you're not admin :(`);
		} else {
			client.say(to,`Yay! You're admin ${from}`);
		}
	})
	.catch( e => {
		client.say(to,`Error: ${e}`);
	});
});
Bot.defineTrigger("debug", null, ev => {
	const {client,message,from,to} = ev;
	
	var tCount = 0;
	for (var trigger in Bot.triggers) {
		tCount++;
	}
	
	client.say(to,`I have ${Bot.regex.length} registered regex calls, and ${tCount} registered trigger calls`);
	
	if (message.length >= 1) {
		if (message[0].equalsIgnoreCase("full")) {
			new Promise( resolve => {
				client.say(to," = Full Debug Start = ");
				client.say(to,"Loaded Regex Strings:");
				resolve();
			})
			.then(()=>{
				Bot.regex.map((obj)=>{
					client.say(to,obj.regex);
				});
				return Promise.resolve();
			})
			.then(()=>{
				client.say(to,`Loaded triggers: ${Object.keys(Bot.triggers).join(", ").trim()}`);
				return true;
			})
			.done(()=>{
				client.say(to," = Full Debug Complete = ");
			});
		}
	}
});

// Custom prototypes
Object.defineProperty(String.prototype, "equalsIgnoreCase", {
	enumerable: false,
	value: function(comp) {
		return this.toString().toLowerCase() === comp.toString().toLowerCase();
	}
});