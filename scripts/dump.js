const Promise = require('promise');

Bot.defineTrigger("dump", null, ev => {
	const {client,message,from,to} = ev;
	
	client.say(to,"Preparing database dump into channel..");
	let data = {};
	Bot.database.db.all("SELECT * FROM sqlite_master where type='table'",[],(err,rows)=>{
		new Promise((resolve)=>{
			return Promise.all(rows.map( (row)=>{
				return new Promise((resolve,reject)=>{
					Bot.database.db.all(`SELECT * FROM ${row.tbl_name};`,[],(err,rows)=>{
						if (err) reject(err);
						console.log("ROW");
						data[row.tbl_name] = rows;
						resolve();
					});
				});
			})).done(()=>{
				console.log("Finished gathering data.");
				resolve();
			});
		}).then(()=>{
			console.log("Pasting data...");
			Bot.lib.pastebin(JSON.stringify(data)).then((url)=>{
				console.log(`Pastebinned to ${url}`);
				client.say(to,`Data dump: ${url}`);
			}).catch((e)=>{
				return Promise.reject(e);
			});
		}).catch((e)=>{
			client.say(to,`Error dumping data: ${e.toString()}`);
		});
	});
});

Bot.defineTrigger("set",null, ev => {
	const {client,message,from,to} = ev;
	
	if (message.length < 1) {
		client.say(to,"Syntax: set [key] [value]");
		return;
	}
	const key = message[0];
	let val = null;
	if (message.length > 1) {
		val = message.splice(1).join(" ");
	}
	const sid = Bot.getSid(client);
	
	new Promise((resolve,reject)=>{
		client.whois(from,(data)=>{
			resolve(data);
		});
	}).then((data)=>{
		const mask = `${data.nick}!${data.user}@${data.host}`;
		
		if (val != null) {
			Bot.database.setKey(mask,sid,key,val).then(()=>{
				client.say(to,`Set ${key} to ${val} in database`);
			}).catch((e)=>{
				client.say(to,`Error: ${e.toString()}`);
			});
		} else {
			Bot.database.removeKey(mask,sid,key).then(()=>{
				client.say(to,`Removed ${key} from database!`);
			}).catch((e)=>{
				client.say(to,`Error: ${e.toString()}`);
			});
		}
	});
});