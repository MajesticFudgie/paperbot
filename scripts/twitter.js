const Twit = require('twit');
const T = new Twit(Bot.keys.twitter);

const stream = T.stream('user', {'with':'user'});

const channels = {};
channels['cakeforce'] = {"#tmfksoft":["MajesticFudgie"]};
channels['jonesey'] = {"#tmfksoft":["MajesticFudgie"]};

stream.on('tweet', function (tweet) {
	let msg = `Twitter: ${tweet.user.name}: ${unParse(tweet.text)} - https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`;
	for (let nid in Bot.clients) {
		let client = Bot.clients[nid];
		if (typeof channels[nid] != "undefined") {
			for (let achn in channels[nid]) {
				for (let chan in client.chans) {
					if (chan.toLowerCase() == achn.toLowerCase()) {
						for (let usr in channels[nid][achn]) {
							user = channels[nid][achn][usr];
							let target = null;
							if (typeof tweet.in_reply_to_screen_name != "undefined") target = tweet.in_reply_to_screen_name;
							if (user.toLowerCase() == tweet.user.screen_name.toLowerCase() || user.toLowerCase() == target.toLowerCase()) {
								client.say(chan,msg);
							}
						}
					}
				}
			}
		}
	}
});
stream.on('error', e => {
	console.log("Whoops! I had an error :<");
	console.log(e);
});

function unParse(msg) {
	return msg.replace(/&lt;/g,"<").replace(/&gt;/g,">");
}