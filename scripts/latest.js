const request = require('request');

Bot.defineTrigger("latest",[], ev => {
	const {client,message,from,to} = ev;
	
	new Promise((resolve,reject)=>{
		request("https://ci.destroystokyo.com/job/PaperSpigot/api/json?pretty=true",(err,resp,body)=>{
			if (!err && resp.statusCode == 200) {
				try {
					body = JSON.parse(body);
					resolve(body);
				} catch (e) {
					reject(err);
				}
			} else {
				reject(err);
			}
		});
	}).then((res)=>{
		var build = res['lastStableBuild'];
		if (message.length >= 1) {
			matches = message[0].match(/git-Paper-([0-9]{4,})/i);
			if (matches != null && matches.length >= 2) {
				if (parseInt(matches[1]) < parseInt(build['number'])) {
					behind = build['number']-matches[1];
					client.say(to,"Latest version: git-Paper-"+build['number']+" ("+build['url']+"), You are "+behind+" versions behind.");
					return;
				} else if (matches[1] == build['number']) {
					client.say(to,"You are up to date!");
					return;
				}
			}
		}
		client.say(to,"Latest version: git-Paper-"+build['number']+" ("+build['url']+")");
	}).catch((err)=>{
		client.say(to,"Error retrieving latest version information.");
		console.log(err);
	});
});