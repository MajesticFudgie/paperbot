const request = require('request');

Bot.defineTrigger("weather", ["w"], ev => {
	const {client,message,from,to} = ev;
	
	client.whois(from,(data)=>{
		let location=null;
		if (message.length != 0) location = message.join(" ");
		
		const mask = `${data.nick}!${data.user}@${data.host}`;
		const network = Bot.getSid(client);
		
		Bot.database.getKey(mask,network,"location").then((d)=>{
			if (location == null) {
				if (d == null) {
					client.say(to,"Syntax: weather [location] -- You can also use the set trigger to set your location for future.");
					return;
				} else {
					location = d.val;
				}
			}
						
			new Promise((resolve,reject)=>{
				request(`http://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${Bot.keys.weather}`,(err,resp,body)=>{
					console.log(body);
					if (!err && resp.statusCode == 200) {
						try {
							body = JSON.parse(body);
							resolve(body);
						} catch (e) {
							reject(err);
						}
					} else {
						reject(err);
					}
				});
			}).then((weather)=>{
				var fahrenheit = 1.8 * (weather.main.temp - 273) + 32;
				var celcius = ((fahrenheit-32)*5)/9;
				client.say(to,`Weather for ${weather.name}, ${weather.weather[0].main} (${weather.weather[0].description}) (${Math.round(fahrenheit)}F / ${Math.round(celcius)}C)`);
			}).catch((e)=>{
				client.say(to,"Error getting weather data :( "+e);
			});
			
			
		});
	});
    
});