const requestp = require('request-promise');
const moment = require('moment');

// Last FM!
// Outputs lastfm info for a user into the channel.

Bot.defineTrigger("lastfm", ["lfm"], ev => {
	const {client,message,from,to} = ev;
	
	client.whois(from, data => {
		const mask = `${data.nick}!${data.user}@${data.host}`;
		const network = Bot.getSid(client);
		
		Bot.database.getKey(mask, network, "lastfm").then( d => {
			let uname;
			
			if (message.length > 0) {
				uname = message[0];
			} else {
				if (d == null) {
					client.say(to,`Usage: lastfm|lfm (username) - Or store a key with the set command!`);
					return;
				} else {
					uname = d.val;
				}
			}
			
			
			getUser(uname)
			.then( data => {
				const {user, track} = data;
				if (track == null) {
					client.say(to,`I couldn't find any Last.FM info for ${(user != null ? user : uname)}.`);
				} else {
					let playing = false;
					if (typeof track['@attr'] != "undefined" && typeof track['@attr'].nowplaying != "undefined") playing = track['@attr'].nowplaying;
					
					let time = "right now";
					if (playing == false) time = moment.unix(track.date.uts).fromNow();
					
					let tense = "was";
					if (playing) tense = "is";
					
					client.say(to,`${user} ${tense} playing "${track.name}" by ${track.artist['#text']} from the album "${track.album['#text']}", ${time} @ ${track.url}`);
				}
			}).catch( e => {
				console.log(e);
				ev.client.say(ev.to,`Error! ${e.message}`);
			});
			
		}).catch(e => {
			ev.client.say(ev.to,`Database Error! ${e}`);
		});
	
	});
});

function getUser(uname) {
	return new Promise( (resolve, reject) => {
		return requestp({url:`http://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user=${uname}&format=json&api_key=${Bot.keys.lastfm}`,json:true})
		.then( data => {
			if (data.error)	return reject(data);
			return data;
		})
		.then( data => {
			const tracks = data.recenttracks.track;
			const who = data.recenttracks['@attr'].user;
			if (tracks.length <= 0) return resolve({user:who,track:null});
			return resolve({user:who,track:tracks[0]});
		})
		.catch( e => {
			reject(e);
		});
	});
}