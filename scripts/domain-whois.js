const whois = require('node-whois');

Bot.defineTrigger("domain", null, ev => {
	const {client,message,from,to} = ev;
	
	new Promise((resolve, reject) => {
		whois.lookup(ev.message[0],(err, data)=>{
			if (err) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	})
	.then( data => {
		return Bot.lib.pastebin(data);
	})
	.then( url => {
		client.say(to, `WHOIS information for ${message[0]} can be found here: ${url}`);
	})
	.catch( e => {
		console.log(e);
		client.say(to, `Error getting WHOIS information for ${message[0]}: ${e.message}`);
	});
});