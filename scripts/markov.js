const pickOneByWeight = require('pick-one-by-weight');
const fs = require('fs');

let wordBank = {};
let sample = fs.readFileSync("data/markov/hhgttg.txt").toString();
sample.split("\n").forEach((scentence)=>{
	teach(scentence);
});

sample = fs.readFileSync("data/markov/shakespeare.txt").toString();
sample.split("\n").forEach((scentence)=>{
	teach(scentence);
});

let talkMode = false;

Bot.defineTrigger("talk", null, ev => {
	const {client,message,from,to} = ev;
	
	talkMode = !talkMode;
	client.say(to,`Talk mode is now ${(talkMode==true ? "on" :" off")}`);
});

Bot.addListener("message", (client,from,to,message) => {
	Log.info(`Teaching "${message}"`);
	teach(message);

	if (talkMode) {
		let ex = message.split(" ");
		let reply = generate(ex.random());
		console.log(reply);
		client.say(to,reply);
	}
});

Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
}

function teach(scentence) {
	if (scentence.trim() == "") return;
	if (scentence.trim().split(" ").length == 1) return;
	
	let ex = scentence.trim().split(" ");
	
	for (let i=0; i < ex.length; i++) {
		if (i == ex.length-1) break;
		
		let count = 0;
		
		const word = ex[i].trim();
		const next = ex[i+1].trim();
		
		if (typeof wordBank[word] != "undefined") {
			if (typeof wordBank[word][next] != "undefined") {
				count = wordBank[word][next];
			}
		} else {
			wordBank[word] = {};
		}
		count++;
		wordBank[word][next] = count;
	}
}
function generate(start) {
	let startWord = Object.keys(wordBank).random();
	if (typeof start != "undefined") startWord = start;
	
	console.log("Start word is "+startWord);
	
	let scentence = [startWord];
	let curWord = startWord;
	
	while (wordBank[curWord] && curWord.slice(-1) != ".") {
		curWord = pickOneByWeight(wordBank[curWord]);
		scentence.push(curWord);
	}
	
	return scentence.join(" ").trim();
}