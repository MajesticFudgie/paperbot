const request = require("request");

Bot.lib.pastebin = function(text) {
	return new Promise((resolve,reject)=>{
		request.post({url:'http://haste.thomas-edwards.me/documents', body:text }, function(err,httpResponse,body){
			if (err) return reject(err);
			body = JSON.parse(body);
			return resolve(`https://haste.thomas-edwards.me/${body.key}`);
		});
	});
}
Bot.defineTrigger("paste",null, ev => {
	const {client,message,from,to} = ev;
	console.log("Attempting to pastebin %",message.join(" ").trim());
	Bot.lib.pastebin(message.join(" ").trim()).then((url)=>{
		client.say(to,url);
	});
});