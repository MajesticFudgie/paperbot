Bot.defineTrigger("where", null, ev => {
	const {client,message,from,to} = ev;
	
	for (var sid in Bot.clients) {
		if (Bot.clients[sid] == client) {
			client.say(to,`You're connected to '${sid}'`);
			break;
		}
	}
});
const tunnels = {};
Bot.defineTrigger("relay", null, ev => {
	const {client,message,from,to} = ev;
	
	Bot.isAdmin(client,from).then((r)=>{
		if (r) return Promise.resolve();
		return Promise.reject("Not admin!");
	}).then(()=>{
		if (message.length <= 0) {
			client.say(to,"Syntax: relay [name]");
			return;
		}
		let relayName = message[0].toLowerCase();
		let create = true;
		if (relayName[0] == "-") {
			create = false;
			relayName = relayName.substr(1);
		}
		
		let sid = null;
		for (var s in Bot.clients) {
			if (Bot.clients[s] == client) {
				sid = s;
				break;
			}
		}
		
		const endpoint = `${sid}:${to.toLowerCase()}`;
		
		if (create) {
			if (typeof tunnels[relayName] != "undefined") {
				if (tunnels[relayName].indexOf(endpoint) < 0) {
					// Add it
					tunnels[relayName].push(endpoint);
					client.say(to,`Added endpoint '${endpoint}' to relay '${relayName}'`);
				} else {
					client.say(to,`This channel has already been added to ${relayName}`);
				}
			} else {
				tunnels[relayName] = [endpoint];
				client.say(to,`Created relay ${relayName}`);
				client.say(to,`Added endpoint '${endpoint}' to relay '${relayName}'`);
			}
		} else {
			if (typeof tunnels[relayName] != "undefined") {
				const index = tunnels[relayName].indexOf(endpoint);
				if (index < 0) {
					// No such endpoint
					client.say(to,`This channel doesn't belong to relay ${relayName}!`);
				} else {
					tunnels[relayName].splice(index,1);
					client.say(to,`Removed endpoint '${endpoint}' from relay '${relayName}'`);
				}
			} else {
				client.say(to,`No such relay ${relayName}!`);
			}
		}
	}).catch((e)=>{
		client.say(to,`Error: ${e}`);
		throw e;
	});
});
Bot.addListener("message",(client,from,to,text)=>{
	
	// Get own sid.
	let sid = null;
	for (var s in Bot.clients) {
		if (Bot.clients[s] == client) {
			sid = s;
			break;
		}
	}
	if (sid==null) return;
	
	// This endpoint
	const endpoint = `${sid}:${to.toLowerCase()}`;
	
	let dest = [];
	
	for (var relay in tunnels) {
		const endpoints = tunnels[relay];
		const index = endpoints.indexOf(endpoint);
		if (index >= 0) endpoints.map((point)=>{
			dest.push(point);
		});
	}
	for (var s in Bot.clients) {
		dest.forEach((point)=>{
			if (endpoint.equalsIgnoreCase(point)) return;
			const ex = point.split(":");
			if (s.equalsIgnoreCase(ex[0])) {
				Bot.clients[s].say(ex[1],`[${endpoint}] (${from}) ${text}`);
			}
		});
	}
});

Bot.addListener("join",(client,to,from,text)=>{
	
	// Get own sid.
	let sid = null;
	for (var s in Bot.clients) {
		if (Bot.clients[s] == client) {
			sid = s;
			break;
		}
	}
	if (sid==null) return;
	
	// This endpoint
	const endpoint = `${sid}:${to.toLowerCase()}`;
	
	let dest = [];
	
	for (var relay in tunnels) {
		const endpoints = tunnels[relay];
		const index = endpoints.indexOf(endpoint);
		if (index >= 0) endpoints.map((point)=>{
			dest.push(point);
		});
	}
	for (var s in Bot.clients) {
		dest.forEach((point)=>{
			if (endpoint.equalsIgnoreCase(point)) return;
			const ex = point.split(":");
			if (s.equalsIgnoreCase(ex[0])) {
				Bot.clients[s].say(ex[1],`[${endpoint}] = ${from} joined the channel =`);
			}
		});
	}
});

Bot.addListener("part",(client,channel,nick,reason,data)=>{
	
	// Get own sid. (To be replaced)
	let sid = null;
	for (var s in Bot.clients) {
		if (Bot.clients[s] == client) {
			sid = s;
			break;
		}
	}
	if (sid==null) return;
	
	// This endpoint
	const endpoint = `${sid}:${channel.toLowerCase()}`;
	
	let dest = [];
	
	for (var relay in tunnels) {
		const endpoints = tunnels[relay];
		const index = endpoints.indexOf(endpoint);
		if (index >= 0) endpoints.map((point)=>{
			dest.push(point);
		});
	}
	for (var s in Bot.clients) {
		dest.forEach((point)=>{
			if (endpoint.equalsIgnoreCase(point)) return;
			const ex = point.split(":");
			if (s.equalsIgnoreCase(ex[0])) {
				if (reason) {
					Bot.clients[s].say(ex[1],`[${endpoint}] = ${nick} parted the channel (${reason.substr(channel.length+1)}) =`);
				} else {
					Bot.clients[s].say(ex[1],`[${endpoint}] = ${nick} parted the channel =`);
				}
			}
		});
	}
});

Bot.addListener("part",(client,channel,nick,by,reason,data)=>{
	
	// Get own sid. (To be replaced)
	let sid = null;
	for (var s in Bot.clients) {
		if (Bot.clients[s] == client) {
			sid = s;
			break;
		}
	}
	if (sid==null) return;
	
	// This endpoint
	const endpoint = `${sid}:${channel.toLowerCase()}`;
	
	let dest = [];
	
	for (var relay in tunnels) {
		const endpoints = tunnels[relay];
		const index = endpoints.indexOf(endpoint);
		if (index >= 0) endpoints.map((point)=>{
			dest.push(point);
		});
	}
	for (var s in Bot.clients) {
		dest.forEach((point)=>{
			if (endpoint.equalsIgnoreCase(point)) return;
			const ex = point.split(":");
			if (s.equalsIgnoreCase(ex[0])) {
				if (reason) {
					Bot.clients[s].say(ex[1],`[${endpoint}] = ${nick} was kicked from the channel by ${by} (${reason.substr(channel.length+1)}) =`);
				} else {
					Bot.clients[s].say(ex[1],`[${endpoint}] = ${nick} was kicked from the channel by ${by} =`);
				}
			}
		});
	}
});

Object.defineProperty(String.prototype, "equalsIgnoreCase", {
	enumerable: false,
	value: function(comp) {
		return this.toString().toLowerCase() === comp.toString().toLowerCase();
	}
});