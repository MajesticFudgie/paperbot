var wolfram = require('wolfram-alpha').createClient(Bot.keys.wolfram, {});
Bot.defineTrigger("wa", null, ev => {
	const {client,message,from,to} = ev;
	
	wolfram.query(message.join(" ").trim(), function (err, result) {
		if (err) throw err;
		result.map((r)=>{
			if (r.primary) {
				const text = r.subpods[0].text.replace(/(\r\n|\n|\r)/gm," ");
				client.say(to,`${r.title}: ${text}`);
			}
		});
	});
});