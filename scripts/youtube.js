const YouTube = require('youtube-node');
const youTube = new YouTube();

const moment = require('moment');
moment().format();

youTube.setKey(Bot.keys.youtube);

const regex = /(?:youtube.*?(?:v=|\/v\/)|youtu\.be\/)([-_a-zA-Z0-9]+)/;
Bot.addListener("message", (client,from,to,message) => {
	const matches = regex.exec(message);
	if (matches == null) return;
	
	const vid = matches[1];
	youTube.getById(vid, function(error, result) {
		if (error) {
			console.log(error);
		}
		else {
			const d = result.items[0];
			let duration = moment.duration(d.contentDetails.duration);
			
			// Build up the duration!
			len = [];
			if (duration.days() > 0) len.push(`${duration.days()}d`);
			if (duration.hours() > 0) len.push(`${duration.hours()}h`);
			if (duration.minutes() > 0) len.push(`${duration.minutes()}m`);
			if (duration.seconds() > 0) len.push(`${duration.seconds()}s`);
			
			
			title = d.snippet.title;
			duration = len.join(" ");
			likes = d.statistics.likeCount;
			dislikes = d.statistics.dislikeCount;
			views = d.statistics.viewCount;
			channel = d.snippet.channelTitle;
			date = moment(d.snippet.publishedAt);
			
			const msg = `${title} - ${duration} - ${likes} likes, ${dislikes} dislikes ~ ${views} views - Posted by ${channel} on ${date}`;
			client.say(to,msg);
			
		}
	});
});