const sqlite3 = require('sqlite3').verbose();

class Database {
	constructor() {
		this.db = new sqlite3.Database('data/database.sqlite');
		
		// Startup proceedure
		this.db.run("CREATE TABLE IF NOT EXISTS user_data (hostmask VARCHAR(20), network VARCHAR(20), key VARCHAR(20), val VARCHAR(20));");
	}
	
	db() {
		return this.db;
	}
	
	setKey(mask,network,key,val) {
		return new Promise((resolve,reject)=>{
			this.db.all("SELECT * FROM user_data WHERE hostmask=? AND network=? AND key=?;",[mask,network,key.toLowerCase()],(err,rows)=>{
				if (err) return resolve(err);
				resolve(rows);
			});
		}).then((rows)=>{
			return new Promise((resolve,reject)=>{
				if (rows.length <= 0) {
					console.log("Inserting key");
					this.db.run("INSERT INTO user_data (hostmask,network,key,val) VALUES(?,?,?,?);",[mask,network,key.toLowerCase(),val],(err)=>{
						if (err) return reject(err);
						resolve();
					});
				} else {
					console.log("Updating key");
					this.db.run("UPDATE user_data SET val=? WHERE hostmask=? AND network=? AND key=?",[val,mask,network,key.toLowerCase()],(err)=>{
						if (err) return Promise.reject(err);
						resolve();
					});
				}
			});
		});
	}
	
	getKey(mask,network,key){
		return new Promise((resolve,reject)=>{
			this.db.all("SELECT * FROM user_data WHERE hostmask=? AND network=? AND key=?;",[mask,network,key.toLowerCase()],(err,rows)=>{
				if (err) return reject(err);
				if (rows.length == 0) {
					resolve(null);
				} else {
					resolve(rows[0]);
				}
			});
		});
	}
	removeKey(mask,network,key){
		return new Promise((resolve,reject)=>{
			this.db.run("DELETE FROM user_data WHERE hostmask=? AND network=? AND key=?;",[mask,network,key.toLowerCase()],(err)=>{
				if (err) return reject(err);
				resolve();
			});
		});
	}
}
module.exports = Database;