const dateFormat = require('dateformat');

class Log {
	constructor(source) {
		this.source = "";
		if (source) this.source = source;
		this.format = "[%date%] [%source:u%] %level% %text%";
		this.date = "h:MMtt mmm d, yyyy";
		this.log = {WARN:[],INFO:[],FATAL:[]};
		this.callback = {WARN:[],INFO:[],FATAL:[],ALL:[]};
	}
	
	on(type,callback) {
		if (typeof this.callback[type.toUpperCase()] == "undefined") return;
		this.callback[type.toUpperCase()].push(callback);
	}
	
	addLine(type,format,args){
		if (typeof this.log[type.toUpperCase()] == "undefined") return;
		
		const line = this.assemble(format,args);
		let logLine = this.format;
		
		const date = dateFormat(new Date(),this.date);
		
		logLine = logLine.replace("%date%",date);
		
		logLine = logLine.replace("%level%",type.toUpperCase());
		
		logLine = logLine.replace("%text%",line);
		
		logLine = logLine.replace("%source%",this.source);
		logLine = logLine.replace("%source:u%",this.source.toUpperCase());
		logLine = logLine.replace("%source:l%",this.source.toLowerCase());
		
		this.log[type.toUpperCase()].push(logLine);
		this.doCallbacks(type,logLine);
	}
	
	doCallbacks(type,line) {
		if (typeof this.callback[type.toUpperCase()] == "undefined") return;
		console.log(line);
		this.callback[type.toUpperCase()].forEach(cb => {
			cb(type,line);
		});
		
		this.callback["ALL"].forEach(cb => {
			cb(type,line);
		});
	}
	
	// This could be done better
	info(abc){
		const args = Array.prototype.slice.call(arguments);
		const format = args[0];
		const elements = args.splice(1);
		this.addLine("INFO",format,elements);
	}
	warn(abc){
		const args = Array.prototype.slice.call(arguments);
		const format = args[0];
		const elements = args.splice(1);
		this.addLine("WARN",format,elements);
	}
	fatal(abc){
		const args = Array.prototype.slice.call(arguments);
		const format = args[0];
		const elements = args.splice(1);
		this.addLine("FATAL",format,elements);
	}
	
	// Does the formatting.
	assemble(format,args) {
		let str = format;
		for (let i=0; i<args.length; i++) {
			str = str.replace('%s',args[i].toString());
		}
		return str;
	}
}
module.exports = Log;